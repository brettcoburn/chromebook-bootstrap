#!/data/data/com.termux/files/usr/bin/env bash
# This sets up a basic Termux environment. It updates and installs core packages, creates a script for Termux:Boot,
# sets up common symlinks, and fetches and runs the other scripts. When this is complete, it enables a chroot environment.

echo "Updating and installing core packages..."

apt update
apt upgrade
apt install coreutils
pkg upgrade
pkg install termux-tools util-linux grep findutils proot man linux-man-pages

echo "Setting wakelock..."
termux-wake-lock

echo "Setting up symlinks..."
termux-setup-storage

echo "Creating Termux:Boot script..."
mkdir -p ~/.termux/boot
cat << EOF > ~/.termux/boot/startup
#!/bin/sh
[ -e $PREFIX/bin/termux-wake-lock ] && echo "[Acquiring wakelock...]" && termux-wake-lock&
EOF

echo "Setting termux-open-url as browser..."
echo "export BROWSER=termux-open-url" >> $HOME/.bashrc

echo "Installing network utility packages..."
pkg install net-tools tracepath curl wget

echo "Downloading bootstrap scripts..."
curl https://gitlab.com/brettcoburn/chromebook-bootstrap/repository/master/archive.tar | tar -x

echo "Entering chroot.."
exec termux-chroot
