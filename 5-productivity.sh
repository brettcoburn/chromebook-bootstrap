#!/data/data/com.termux/files/usr/bin/env bash
# Installs productivity tools

echo "Installing productivity tools..."
pkg install taskwarrior tasksh timewarrior gcal remind calcurse

#TODO
#basic remind config
#calcurse / GCal integration helper? Break out into separate script?
