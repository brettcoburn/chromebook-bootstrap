#!/data/data/com.termux/files/usr/bin/env bash
# Set up python dev environment

echo "Installing python dev environment..."

pkg install python python-dev python2 python2-dev vim-python

echo "Setting up virtualenv..."
pip2 install virtualenv
pip2 install virtualenvwrapper
pip3 install virtualenv
pip3 install virtualenvwrapper

export WORKON_HOME=$HOME/.virtualenvs
source $PREFIX/bin/virtualenvwrapper.sh

cat << EOF >> .bashrc
export WORKON_HOME=$HOME/.virtualenvs
source $PREFIX/bin/virtualenvwrapper.sh
EOF

