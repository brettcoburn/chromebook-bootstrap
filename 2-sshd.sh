#!/data/data/com.termux/files/usr/bin/env bash
# Installs OpenSSH, creates key pair, and builds login profile for SSH sessions.

echo "Installing SSH..."
pkg install openssh

echo "\nGenerating SSH keys... (please follow prompts)"
echo -n "Enter your email address: "
read email
mkdir -p ~/sshkeys
ssh-keygen -t rsa -b 4096 -C "$email" -f ~/sshkeys/id_rsa

if [ $? -eq 0 ]; then
  # Authorize the newly created key (note: remote access isn't possible, can only access from local Chromebook environment)
  cat ~/sshkeys/id_rsa.pub >> ~/.ssh/authorized_keys

  # Add sshd to startup
  echo "[ -e $PREFIX/bin/sshd ] && echo \"[Starting sshd in chroot...]\" && termux-chroot sshd&" >> ~/.termux/boot/startup

  echo "Starting sshd in chroot..."
  termux-chroot sshd&

  echo "\n======================================================================="
  echo "\nYour SSH keys have been generated in the ~/sshkeys/ folder. If using"
  echo "Termux on Chrome OS, copy your keys to Chrome OS using your transfer"
  echo "folder or by copying them to Downloads using ~/storage/downloads/."
  echo "\nUse the following username and IP to connect:"
  echo -n "User: "
  whoami
  echo -n "IP: "
  ifconfig arc0 | awk '/inet /{print $2}'
  echo "\n======================================================================="
else
  echo "Key generation failed. Please try again."
fi
