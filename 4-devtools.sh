#!/data/data/com.termux/files/usr/bin/env bash
# Install basic dev tools

echo "Installing development tools..."

pkg install nano vim tracepath git pastebinit make mosh strace tree htop
