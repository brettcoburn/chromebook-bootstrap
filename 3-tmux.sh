#!/data/data/com.termux/files/usr/bin/env bash
# Installs tmux and enables automatic tmux session on SSH login.

echo "Installing and configuring tmux..."
pkg install tmux

# Set up tmux session on SSH login - could use $USER instead of ssh_tmux if you prefer
cat << EOF >> ~/.bashrc
if [[-z "TMUX"]] && ["SSH_CONNECTION" != ""]; then
  tmux attach-session -t ssh_tmux || tmux new-session -s ssh_tmux
fi
EOF
