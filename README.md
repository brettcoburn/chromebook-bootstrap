# chromebook-bootstrap

**Scripts to set up a Termux-based development environment on Chrome OS (or Android, probably)**

Still a work in progress, but it's a start. This work was inspired by [My $169 development Chromebook](https://blog.lessonslearned.org/building-a-more-secure-development-chromebook/) by Kenneth White.

## Prerequisites

1. Acquire a Chromebook [capable](https://www.chromium.org/chromium-os/chrome-os-systems-supporting-android-apps) of running Android apps.
2. [Set up](https://support.google.com/chromebook/answer/7021273) the Play Store on the Chromebook.
3. Install the [Termux](https://play.google.com/store/apps/details?id=com.termux&hl=en) app from the Play Store.
4. If you wish to run termux-chroot and sshd automatically at boot (recommended), purchase and install [Termux:Boot](https://play.google.com/store/apps/details?id=com.termux.boot&hl=en).

## Usage

1. Run `apt install curl`. (The busybox wget version included in Termux does not support SSL.)
2. Run `curl -O https://brettcoburn.gitlab.io/chromebook-bootstrap/0-bootstrap.sh` to download the bootstrapper script.
3. Run the bootstrapper script with `sh ./0-bootstrap.sh`. This will prepare your environment and download the other scripts.
4. Run each downloaded script in order. You can skip scripts that are not relevant to you.

## SSH access

The Termux app is not an ideal terminal under Chrome OS. I personally found it to have issues with lag, and copy/paste did not work well. The best option is to install [Secure Shell](https://chrome.google.com/webstore/detail/secure-shell/pnhechapfaindjhompbnflcldabbghjo), [FireSSH](https://chrome.google.com/webstore/detail/firessh/mcognlamjmofcihollilalojnckfiajm), or another native Chrome OS app for SSH access.

Inside the Termux sandbox, your SSH terminal is isolated on a private IP address behind a local NAT, so this is pretty secure. (Theoretically it might be possible for a malicious app to attack your Termux instance, so don't install any of those. It would also be prudent to turn off app sync if you are paranoid.)

## What does each script do?

You should read each script to understand what it does, but here's a quick description of each:

 - 0-bootstrap.sh
    - Updates Termux packages
    - Installs core utilities
    - Sets up symlinks for ease of use
    - Sets up Termux:Boot scripts
    - Acquires the latest bootstrap scripts from the GitLab repository
 - 1-create-transfer-folder.sh
    - Creates transfer folders for moving files to and from Chrome OS
 - 2-sshd.sh
    - Installs sshd for local SSH access using Chrome apps
    - Generates SSH keys
    - Adds SSH to Termux:Boot script
 - 3-tmux.sh 
    - Installs tmux and sets up tmux session for SSH logins
 - 4-devtools.sh
    - Installs common development tools (vim, nano, git, make, etc...)
 - 4a-python.sh
    - Sets up a functional (but basic) Python environment with virtualenv support
 - 5-productivity-tools.sh
    - Installs various task, time tracking, and calendar tools

## Why would I need this?

The idea behind these scripts is to give you a way to rapidly redeploy a secure, barebones development 
environment should you need one in a hurry. Ideal usage scenarios include cross-border travel, replacing lost or 
damaged equipment, rapid deployment for new employees, and so on. It also provides a great way to quickly
investigate Termux as a development environment. 

For the most part, these scripts should also work for Android-based Termux development. I will work on improving
Android compatibility eventually...

## Notes on security

First of all, you should never run random scripts posted by some guy on the internet. That said, what's great about 
this setup is that Chrome OS and Termux provide an easily refreshed environment. If you decide that you don't 
like the results, just wipe it out and start over. Or if you're really paranoid, you can always powerwash the 
Chromebook or even reflash it using the Chrome OS Recovery utility. (That said, *please* read the scripts before 
you run them blindly.)

As an aside, it would be really hard -- though certainly not impossible -- to do anything truly harmful to Chrome OS
from within Termux. Google does an incredible job at [locking down](https://www.chromium.org/chromium-os/chromiumos-design-docs/system-hardening) the environment. That may change as Chromebooks
become a more attractive target, but for now it's a great choice if you want a hardened dev platform.

## Other resources for Chromebook-based development

* The [ONCGen](https://unfix.org/projects/chromeos-openvpn-onc/) project may also be useful for getting OpenVPN up and running.
* [cros-pia](https://chromium.googlesource.com/experimental/cros-pia/) provides ONC files for use with the Private Internet Access VPN service.
* [termux-bootstrap](https://github.com/xovertheyearsx/termux-bootstrap) has a similar approach, but a more complex implementation. (At present, it doesn't provide any helper scripts for Chrome OS deployments either.)
