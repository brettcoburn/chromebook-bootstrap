#!/data/data/com.termux/files/usr/bin/env bash
# Sets up a transfer folder for moving files from Chrome OS <-> Termux.

echo -n "Enter the name of the folder you want to create: "
read transferfolder
transferfolder=`bash --norc -c "printf '%q' \"$transferfolder\""` # Invoke bash specifically in case we are running from sh
[ -z "$transferfolder" ] && echo "ERROR: You must enter a folder name!" && exit

mkdir /storage/emulated/0/Download/$transferfolder
ln -s /storage/emulated/0/Download/$transferfolder ~/$transferfolder

echo "You can now use Downloads/$transferfolder/ in Chrome OS to transfer files to and from Termux."
